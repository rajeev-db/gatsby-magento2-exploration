import wrapWithProvider from './redux-provider';

export const wrapPageElement = wrapWithProvider;
