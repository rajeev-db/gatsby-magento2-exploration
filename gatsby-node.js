const path = require('path');

const apolloClient = require('./src/graphql/node-client');
const { GET_ALL_CATEGORIES, GET_ALL_PRODUCTS } = require('./src/graphql/node-query');
const { query } = require('./src/graphql/utilities');
const fetchReduxData = require('./src/store/collect-preload-state')

/**
 * Create pages for all of our site categories and products
 *
 */
exports.createPages = async ({ actions: { createPage } }) => {
    const allCategories = [];
    const allProducts = [];

    // helper function: use to create category pages
    function createCategoryPages(catgoryItems) {
        let childCategories = [];

        catgoryItems.forEach(categoryItem => {
            const {
                include_in_menu: canInclude,
                url_key: urlKey,
                id: pageCategoryId,
                children = [],
            } = categoryItem;

            childCategories = [
                ...childCategories,
                ...children,
            ];

            if (canInclude) {
                allCategories.push({ id: pageCategoryId, url: urlKey });
                createPage({
                    path: urlKey,
                    component: path.resolve('./src/pages/ecommerce.js'),
                    context: { pageCategoryId }
                });
            }
        });

        return childCategories;
    }

    //_______________________ CATEGORY PAGES GENERATION _______________________
    const {
        data: {
            categoryList: {
                0: {
                    children: categories = { categories: [] },
                },
            },
        }
    } = await query(apolloClient.query({ query: GET_ALL_CATEGORIES }));

    if (categories.length) {
        createCategoryPages(createCategoryPages(categories));
    }

    //_______________________ PRODUCT PAGES GENERATION _______________________
    const {
        data: {
            products: {
                items: productList = { productList: [] },
            }
        }
    } = await query(apolloClient.query({ query: GET_ALL_PRODUCTS }));

    if (productList.length) {
        productList.forEach(({ sku, url_key: urlKey }) => {
            allProducts.push({ sku, url: urlKey });
            createPage({
                path: urlKey,
                component: path.resolve('./src/pages/ecommerce/product.js'),
                context: { pageProductSku: sku },
            });
        });
    }

    //___________________ FETCHING REDUX DATA ______________________________
    fetchReduxData({ categories: allCategories, products: allProducts });
}
