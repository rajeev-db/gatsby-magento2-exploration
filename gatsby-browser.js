import wrapWithProvider from './redux-provider';

/**
 * As per gatsby documentation, redux provider actually need to inject through
 *  "wrapRootElement" api. But to prepare store data, it is essential to have
 * route path information. "wrapRootElement" lags this information when it
 * comes to this file while it is available in gatsby-ssr.js
 *
 * So we are using this hook to inject redux provider in both ssr and browser
 * side. I hope everything will be alright!!!
 */
export const wrapPageElement = wrapWithProvider;
