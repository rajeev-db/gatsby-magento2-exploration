import React from "react";
import { Provider } from "react-redux"

import createStore from "./src/store/store";
import getSSRStates from "./src/store/preload-states";



// eslint-disable-next-line react/display-name,react/prop-types
export default ({
    element,
    props: {
        location: {
            pathname = ''
        }
    }
}) => {
    const store = createStore(getSSRStates(pathname));
    return <Provider store={store}>{element}</Provider>;
}
