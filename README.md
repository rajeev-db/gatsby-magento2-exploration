# Gatsby & Magento 2 Puzzle

This project is trying to use Magento 2 with Gatsby.

Gatsby is a React-based, GraphQL powered, static site generator.
Magento 2 is an ecommerce CMS

## Why we are trying this?

- Magento 2 is great in many aspect, but it lags in one area - SPEED
- It is slow because, it loads tons of javascript files and css files for a single page
- A magento 2 sites without proper cache machanism will crawl like snail

## Why gatsby

- Gatsby is static site generator, build for speed
- Gatsby uses GraphQL with wich we can connect to magento 2 flawless (headless Magento)
- Gatsby is react based. So we liverage all the power of modern javascript (es6)

## What tools we are using

- Gatsby: https://www.gatsbyjs.org/
- TMART theme: http://themehunt.com/item/1527068-tmart-free-minimal-ecommerce-html5-template/preview
- Apollo Client: https://www.apollographql.com/docs/react/
- Redux: https://redux.js.org/

## How to setup

1. Make your Magento 2 site CORS enabled. For this you can use this module: https://github.com/graycoreio/magento2-cors . Please follow instructions there to setup the module there
2. Specify the magento 2 graphql endpoint in gatsby project. For this update `uri` variable in the files
   `src/graphql/client.js` and `src/graphql/node-client.js`.
3. cd into gatsby project root directory. run `npm i`. This will add all the npm packages
4. now either run `gatsby build` or `gatsby develop`

## Need to improve

- I am using TMART template for quickly getting a UI. This is bootstrap based theme. We are now using css
  globally. This is a big no no. In an actual project, we will manage css in component level.
- Seems gatsby has built in image management which i didnt explore much. Right now I am referring
  images directly from the Magento 2 site. However we can optimize in Magento 2 level too.

  Not sure which one of the option suits most. My bet would be gatsby apporach since it may improve user
  experience
  
- Gatsby build process is unique feature. We will be using the build process to generate pages for all the
  categories and products.

  We are also using server-side-rendering feature that is provided by gatsby out of box. We need to fetch
  required backend data in order to correctly generate static pages for all categories and products.

  So based on the number of products, number of categories and number of stores in a Magento 2 site, the build
  time process can be higher.

- Magento 2 graphql support is not mature enough. This means we need to create custom graphql queries, mutations
  for "filling the blanks"
  
- Need to each component from the scratch. This would take time

## Advatages

- Blazing fast!!
- Seems graphql calls (without cacheing) is performing much better than a typical magento 2 frontend theme
- Easy to develop. Use of graphql allows developers to work independently. Graphql demands a well
  defined schema in its score. This means both frontend developer and backend developer knows how data is going
  to looks like.
- Graphql calls can be cached either by apollo or by magento 2 varnish
- User experience is way better
