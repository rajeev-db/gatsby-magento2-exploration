import { combineReducers } from 'redux';

import pdp from './pdp';
import cart from './cart';
import store from './store';
import customer from './customer';
import header from './header';
import menu from './menu';
import category from './category';
import message from './message';

const rootReducer = combineReducers({
    cart,
    category,
    customer,
    header,
    menu,
    message,
    pdp,
    store,
});

export default rootReducer;
