import { _inArray } from '../../components/helper';
import {
    UPDATE_CART_ID,
    SET_CART_INFO,
} from "../type";

export const cartInitialState = {
    cartId: '', // 275365
    info: {},
};

export default (state = cartInitialState, { type, payload }) => {
    if (_inArray(
        [
            UPDATE_CART_ID,
            SET_CART_INFO
        ],
        type
    )) {
        return {
            ...state,
            ...payload,
        };
    } else {
        return state;
    }
}
