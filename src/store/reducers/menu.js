import { SET_MENU_ITEMS } from "../type";

export const menuReducerInitialState = {
    items: [],
};

export default (state = menuReducerInitialState, { type, payload}) => {
    if (type === SET_MENU_ITEMS) {
        return {
            ...state,
            ...payload,
        };
    } else {
        return state;
    }
};
