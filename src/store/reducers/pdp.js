import ProductSwatchModel from '../../components/model/product/swatch';
import {
    ADD_PDP_SWATCH_SELECTED,
    UPDATE_PDP_QTY_SELECTED,
    SET_PDP_SWATCH_ATTRIBUTE_IDS,
    UPDATE_PDP_SKU,
    SET_PDP_PRODUCT,
} from '../type';
import { _inArray } from '../../components/helper';

export const pdpReducerInitialState = {
    productList: {},
    sku: '',
    swatchAttributeIds: [],
    swatchesSelected: [],
    qtySelected: 0,
};

export default (state = pdpReducerInitialState, { type, payload }) => {
    if (type === ADD_PDP_SWATCH_SELECTED) {
        return {
            ...state,
            swatchesSelected: ProductSwatchModel.prepareSelectedSwatches(
                state.swatchesSelected,
                payload
            ),
        };

    } else if (type === SET_PDP_PRODUCT) {
        return {
            ...state,
            productList: {
                ...state.productList,
                [payload.sku]: payload,
            },
        };

    } else if (_inArray(
        [
            UPDATE_PDP_QTY_SELECTED,
            SET_PDP_SWATCH_ATTRIBUTE_IDS,
            UPDATE_PDP_SKU,
        ],
        type
    )) {
        return {
            ...state,
            ...payload,
        };

    } else {
        return state;
    }
}
