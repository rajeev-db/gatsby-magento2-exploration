import { UPDATE_CATEGORY_ID } from "../type";

export const categoryReducerInitialState = {
    categoryId: '14',
    products: [],
};

export default (state = categoryReducerInitialState, { type, payload }) => {
    if (type === UPDATE_CATEGORY_ID) {
        return {
            ...state,
            ...payload,
        };
    } else {
        return state;
    }
};
