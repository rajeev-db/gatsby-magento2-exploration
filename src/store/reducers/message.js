import { SET_MESSAGE } from "../type";

const initialState = {
    type: '',
    message: '',
};

export default function(state = initialState, { type, payload}) {
    if (type === SET_MESSAGE) {
        return {
            ...state,
            ...payload,
        };
    }

    return state;
}
