import { UPDATE_HEADER_MINICART_STATUS } from "../type";

const initialState = {
    minicartStatus: false,
};

export default (state = initialState, { type, payload }) => {
    if (type === UPDATE_HEADER_MINICART_STATUS) {
        return {
            ...state,
            ...payload,
        };
    } else {
        return state;
    }
};
