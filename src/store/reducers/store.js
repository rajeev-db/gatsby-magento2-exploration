import { UPDATE_STORE_CURRENCY_SYMBOL } from "../type";

export const storeReducerInitialState = {
    currencySymbol: '',
};

export default (state = storeReducerInitialState, { type, payload }) => {
    if (type === UPDATE_STORE_CURRENCY_SYMBOL) {
        return {
            ...state,
            ...payload,
        };
    } else {
        return state;
    }
}
