import client from '../../graphql/client';
import { QUERY_GET_PRODUCT_DETAILS_BY_SKU } from '../../graphql/query';
import {
    ADD_PDP_SWATCH_SELECTED,
    UPDATE_PDP_QTY_SELECTED,
    SET_PDP_SWATCH_ATTRIBUTE_IDS,
    UPDATE_PDP_SKU,
    SET_PDP_PRODUCT,
} from '../type';
import { updateStoreCurrencySymbol } from './store';

export function addPDPSwatchSelected(swatchId) {
    return {
        type: ADD_PDP_SWATCH_SELECTED,
        payload: swatchId,
    };
}

export function updatePDPQtySelected(qty) {
    return {
        type: UPDATE_PDP_QTY_SELECTED,
        payload: { qtySelected: qty },
    };
}

export function setPDPSwatchAttributeIds(attrIds) {
    return {
        type: SET_PDP_SWATCH_ATTRIBUTE_IDS,
        payload: { swatchAttributeIds: attrIds },
    }
}

export function updatePDPCurrentProduct(sku) {
    return {
        type: UPDATE_PDP_SKU,
        payload: { sku }
    }
}

export function setPDPProductDetails(product) {
    return {
        type: SET_PDP_PRODUCT,
        payload: product,
    };
}

export function fetchProductDetails(sku) {
    return dispatch => client
        .query({ query: QUERY_GET_PRODUCT_DETAILS_BY_SKU, variables: { productSku: sku } })
        .then(({ data }) => {
            const {
                currency: {
                    default_display_currency_symbol: currencySymbol = { currencySymbol: ''},
                },
                products: {
                    items:  {
                        0: productDetails = { productDetails: false },
                    }
                },
             } = data;

            dispatch(updateStoreCurrencySymbol(currencySymbol));

            if (productDetails) {
                dispatch(setPDPProductDetails(productDetails));
            }

            if (productDetails && productDetails.configurable_options) {
                const swatchAttributes =  productDetails.configurable_options.map(
                    op => op.attribute_id
                );
                dispatch(setPDPSwatchAttributeIds(swatchAttributes));
            }
        });
}
