import client from '../../graphql/client';
import { QUERY_LEVEL_3_CATEGORY_LIST } from "../../graphql/query";
import { SET_MENU_ITEMS } from "../type";

export function setMenuItems(items) {
    return {
        type: SET_MENU_ITEMS,
        payload: { items },
    };
}

export function fetchCategoriesLevel3() {
    return dispatch => client
        .query({ query: QUERY_LEVEL_3_CATEGORY_LIST })
        .then(({ data }) => {
            const { categoryList: { 0: category2 = {} }} = data;
            const { children: categoriesLevel3  = [] } = category2;

            dispatch(setMenuItems(categoriesLevel3.filter(cat => cat.include_in_menu)))
        });
}
