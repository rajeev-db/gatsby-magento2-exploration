import { UPDATE_STORE_CURRENCY_SYMBOL } from '../type';

export function updateStoreCurrencySymbol(symbol) {
    return {
        type: UPDATE_STORE_CURRENCY_SYMBOL,
        payload: { currencySymbol: symbol },
    };
}
