import { UPDATE_HEADER_MINICART_STATUS } from "../type";

export function updateHeaderMinicartStatus(status) {
    return {
        type: UPDATE_HEADER_MINICART_STATUS,
        payload: { minicartStatus: status },
    };
}
