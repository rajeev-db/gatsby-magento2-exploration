import { SET_MESSAGE } from "../type";

export function setGlobalMessage(message, type = 'success',) {
    return {
        type: SET_MESSAGE,
        payload: { type,  message },
    };
}
