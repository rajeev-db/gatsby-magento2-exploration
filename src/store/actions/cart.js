import client from '../../graphql/client';
import { addToStorage } from '../localStorage';
import {
    CREATE_CUSTOMER_EMTPY_CART,
    CREATE_GUEST_EMPTY_CART,
    ADD_CONFIG_PRODUCT_TO_CART,
    QUERY_GET_CART_INFO,
} from '../../graphql/query';
import {
    UPDATE_CART_ID,
    SET_CART_INFO,
} from '../type';


export function updateCartId(cartId) {
    return {
        type: UPDATE_CART_ID,
        payload: { cartId },
    };
}

export function setCartInfo(info) {
    return {
        type: SET_CART_INFO,
        payload: { info },
    };
}

export function addProductoCart(cartInfo) {
    return dispatch => {
        return client
            .mutate({
                mutation: ADD_CONFIG_PRODUCT_TO_CART,
                variables: cartInfo,
            })
            .then(({ data: { addConfigurableProductsToCart: { cart }} }) => {
                dispatch(setCartInfo(cart));
            });
    };
}

export function createEmptyCart(isLoggedIn) {
    return dispatch => {
        if (isLoggedIn) {
            return client
                .mutate({
                    mutation: CREATE_CUSTOMER_EMTPY_CART
                });
        } else {
            return client
                .mutate({
                    mutation: CREATE_GUEST_EMPTY_CART
                })
                .then(({ data: { createEmptyCart: guestCartKey } }) => {
                    dispatch(updateCartId(guestCartKey));
                    addToStorage({
                        cart: {
                            cartId: guestCartKey,
                        },
                    });
                    return guestCartKey;
                });
        }
    };
}

export function fetchCartDetails() {
    return (dispatch, getState) => {
        const { cart: { cartId } } = getState();

        return client
            .query({
                query: QUERY_GET_CART_INFO,
                variables: { cartId },
            })
            .then(({ data: { cart } }) => {
                dispatch(setCartInfo(cart));
            });
    }
}
