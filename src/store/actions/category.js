import client from '../../graphql/client';
import { QUERY_GET_CATEGORY_PRODUCTS } from '../../graphql/query';
import { updateStoreCurrencySymbol } from './store';
import { UPDATE_CATEGORY_ID } from '../type';

export function updateCategoryId(categoryId) {
    return {
        type: UPDATE_CATEGORY_ID,
        payload: { categoryId }
    };
}

export function fetchCategoryProducts() {
    return (dispatch, getState) => {
        const { category: { categoryId } } = getState();

        return client
                .query({ query: QUERY_GET_CATEGORY_PRODUCTS, variables: { categoryId } })
                .then(({ data: { currency, products } }) => {
                    dispatch(updateStoreCurrencySymbol(currency.default_display_currency_symbol));
                    return products;
                });
    }
}
