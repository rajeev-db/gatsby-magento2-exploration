import { cartInitialState } from "./reducers/cart";

export const loadState = () => {
    try {
        const serializedState = localStorage.getItem('ecommerce');
        if (serializedState === null) {
            return undefined;
        }
        const localStorageData = JSON.parse(serializedState);
        return {
            ...localStorageData,
            cart: {
                ...cartInitialState,
                ...localStorageData.cart,
            },
        };
    } catch (err) {
        return undefined;
    }
};

export const saveState = state => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('ecommerce', serializedState);
    } catch {
        // ignore write errors
    }
};

export const addToStorage = newData => {
    if (typeof newData !== 'object') {
        return;
    }

    const existingData = loadState() || {};
    saveState({ ...existingData, ...newData });
};
