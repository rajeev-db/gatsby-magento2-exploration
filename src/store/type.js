//_______________________ PDP __________________________
export const ADD_PDP_SWATCH_SELECTED = 'ADD_PDP_SWATCH_SELECTED';
export const UPDATE_PDP_QTY_SELECTED = 'UPDATE_PDP_QTY_SELECTED';
export const SET_PDP_SWATCH_ATTRIBUTE_IDS = 'SET_PDP_SWATCH_ATTRIBUTE_IDS';
export const UPDATE_PDP_SKU = 'UPDATE_PDP_SKU';
export const SET_PDP_PRODUCT = 'SET_PDP_PRODUCT';

//______________________ STORE __________________________
export const UPDATE_STORE_CURRENCY_SYMBOL = 'UPDATE_STORE_CURRENCY_SYMBOL';

//______________________ CART __________________________
export const UPDATE_CART_ID = 'UPDATE_CART_ID';
export const SET_CART_INFO = 'SET_CART_INFO';

//______________________ HEADER __________________________
export const UPDATE_HEADER_MINICART_STATUS = 'UPDATE_HEADER_MINICART_STATUS';

export const SET_MENU_ITEMS = 'SET_MENU_ITEMS';

export const UPDATE_CATEGORY_ID = 'UPDATE_CATEGORY_ID';

export const SET_MESSAGE = 'SET_MESSAGE';
