const fs = require('fs');
const apolloClient = require('../graphql/node-client');
const util = require("util");
const writeFileAsync = util.promisify(fs.writeFile);
const mkDirAsync = util.promisify(fs.mkdir);
const {
    COLLECT_GENERAL,
    COLLECT_CATEGORY_PRODUCTS,
    COLLECT_PRODUCT_DETAILS,
} = require('../graphql/query/giant-query');

const DIR = `${__dirname}/.tmp/data`;
const MENU_FILE = 'menu.json';
const CATEGORY_FILE = 'categories.json';
const STORE_FILE = 'store.json';
const PRODUCTS_FILE = 'products.json';
const FILES = [MENU_FILE, CATEGORY_FILE, STORE_FILE, PRODUCTS_FILE];


// _____________ LET US CREATE ALL NECESSARY JSON FILES BEFORE HAND _____________________
async function createFiles() {
    const writeFiles = () => {
        FILES.forEach(async file => {
            await writeFileAsync(`${DIR}/${file}`, JSON.stringify({}), error => {
                if (error) {
                    console.log(error)
                }
            });
        })
    };

    if (!fs.existsSync(DIR)) {
        await mkDirAsync(DIR, { recursive: true }, writeFiles)
    } else {
       writeFiles();
    }
}

// ________________________________ JSON FILE WRITERS _____________________________________
function writeIntoFile(file, data = {}) {
    writeFileAsync(
        `${DIR}/${file}`,
        JSON.stringify(data, null, 2),
        error => {
            if (error) {
                console.log('cannot write file:', file, error);
            }
        }
    );
}

function writeMenuStoreData(data) {
    const {
        categoryList: {
            0: {
                children: categoriesLevel3 = { categoriesLevel3: [] },
            }
        }
    } = data;

    writeIntoFile(MENU_FILE, {
        menu: {
            items: categoriesLevel3.filter(cat => cat.include_in_menu),
        },
    });
}

function writeStoreStoreData(data) {
    const {
        currency: {
            default_display_currency_symbol: currencySymbol = { currencySymbol: '' },
        }
    } = data;
    writeIntoFile(STORE_FILE, {
        store: {
            currencySymbol,
        },
    });
}

function writeCategoryStoreData(data) {
    const { categoryList = [] } = data;

    writeIntoFile(CATEGORY_FILE, {
        categoryProducts: categoryList,
    });
}

function writeProductDetailsStoreData(data) {
    const { products = [] } = data;

    writeIntoFile(PRODUCTS_FILE, {
        productDetails: products,
    });
}

// _________________________DATA COLLECTORS aka GRAPHQL QUERIES _____________________________________
function collectGeneralData() {
    apolloClient
        .query({ query: COLLECT_GENERAL })
        .then(({ data: queryResult}) => {
            if (queryResult) {
                writeMenuStoreData(queryResult);
                writeStoreStoreData(queryResult);
            }
        });
}

function collectCategoryProducts(categoryList = []) {
    if (!categoryList.length) {
        return;
    }

    const categoryIds = categoryList.map(({ id }) => id);

    apolloClient
        .query({ query: COLLECT_CATEGORY_PRODUCTS, variables: { ids: categoryIds } })
        .then(({ data: queryResult }) => {
            if (queryResult) {
                writeCategoryStoreData(queryResult);
            }
        });
}

function collectProductDetails(productList = []) {
    if (!productList.length) {
        return;
    }

    const productSkus = productList.map(({ sku }) => sku.toString());
    const variables = { skus: productSkus, page: productSkus.length };

    apolloClient
        .query({
            query: COLLECT_PRODUCT_DETAILS,
            variables
        })
        .then(({ data: queryResult }) => {
            if (queryResult) {
                writeProductDetailsStoreData(queryResult);
            }
        }).catch(error => console.log('products fetch', error));
}

/**
 * MAIN FUNCTION
 *
 * Fetching init redux states for the app during building time.
 * it will later use by gatsby-browser.js and gatsby-ssr.js APIs
 *
 * We are now writing down these data into json files which you can
 * find under `src/store/.tmp/data`. I could not find a better alternative
 * for this approach.
 *
 * Our requirement is, we need to collect all the details of page generation
 * before gatsby process our gatsby-ssr.js. I tried to do the job there  but,
 * it seems asynchronous calls wont work there. Total desperation :(
 *
 * So before gatsby start to generate static pages, we are now collecting
 * details here and saving into json file and then refer these json files
 * in gatsby-ssr.js.
 *
 * I dont feel this is a good apporach since there is a chance when all of the
 * json files are not available when gatsby reaches static page generation
 */
module.exports = function fetchReduxData({ categories, products }) {
    try {
        createFiles();
        collectGeneralData();
        collectCategoryProducts(categories);
        collectProductDetails(products)
        return true;
    } catch (error) {
        console.log('collect-preload-state', error)
        return true;
    }
}
