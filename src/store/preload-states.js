import {
    categoryReducerInitialState,
    pdpReducerInitialState,
    storeReducerInitialState,
    menuReducerInitialState,
} from './reducers/initial-states';

// json files generating during build
import menuJson from './.tmp/data/menu.json';
import categoriesJson from './.tmp/data/categories.json';
import storeJson from './.tmp/data/store.json';
import productDetailsJson from './.tmp/data/products.json';

const { categoryProducts = [] } = categoriesJson;
const {
    productDetails: {
        items: productDetails = { productDetails: [] },
    },
} = productDetailsJson || {};

const _isUriEqualChecker = pathname => ({ url_key }) => pathname === `/${url_key}`;
const _prependSlashOnUri = ({ url_key }) => `/${url_key}`;

const categoryPaths = categoryProducts.map(_prependSlashOnUri);
const productsPaths = productDetails.map(_prependSlashOnUri);

function prepareMenuStates() {
    return {
        ...menuReducerInitialState,
        ...((menuJson || {}).menu || {}),
    };
}

function prepareStoreStates() {
    return {
        ...storeReducerInitialState,
        ...((storeJson || {}).store || {}),
    };
}

function preparePDPStates(pathname) {
    if (!productsPaths.includes(pathname)) {
        return pdpReducerInitialState;
    }

    const pageProductDetail = productDetails.find(_isUriEqualChecker(pathname));

    if (pageProductDetail) {
        return {
            ...pdpReducerInitialState,
            sku: pageProductDetail.sku,
            productList: {
                ...pdpReducerInitialState.productList,
                [pageProductDetail.sku]: pageProductDetail,
            },
        };
    }

    return pdpReducerInitialState;
    // if (productDetails && productDetails.configurable_options) {
    //     const swatchAttributes =  productDetails.configurable_options.map(
    //         op => op.attribute_id
    //     );
    //     updateSwatchAttributes(swatchAttributes);
    // }
}

function preparePLPStates(pathname) {
    if (!categoryPaths.includes(pathname)) {
        return categoryReducerInitialState;
    }

    const {
        products: {
            items: categoryProductList = { categoryProductList: [] },
        },
    } = categoryProducts.find(_isUriEqualChecker(pathname)) || {};

    if (categoryProductList) {
        return {
            ...categoryReducerInitialState,
            products: categoryProductList,
        };
    }

    return categoryReducerInitialState;
}

/**
 * Prepare initial redux store information
 *
 * During build process, we will collect necessary information to prepare the initial store
 * information and saves under `./src/store/.tmp/data` folder. All these temporary data are
 * in json format.
 *
 * gatsby-browser.js and gatsby-ssr.js will call this function to create store for each pages
 * later and we will prepare page specific initial redux state here
 *
 * Seems asynchronous fetching of data from the APIs providing in this file is impossible to
 * to achieve. That is why we are writing data into json files ahead.
 *
 * Let me know a better approach is possible here
 *
 */
export default function getSSRStates(pathname = '') {
    console.log(pathname);
    return {
        menu: prepareMenuStates(),
        store: prepareStoreStates(),
        category: preparePLPStates(pathname),
        pdp: preparePDPStates(pathname),
    }
}
