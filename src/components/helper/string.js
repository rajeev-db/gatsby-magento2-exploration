function _removeNums(string) {
    return Array.from(string || '', s => !!Number(s) ? '': s).join('');
}
