
export function _inArray(arr, value) {
    return (arr || []).includes(value);
}

export function _chunks(arr, size) {
    const newArr = [...arr];
    return Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
        newArr.slice(i * size, i * size + size)
    );
}
