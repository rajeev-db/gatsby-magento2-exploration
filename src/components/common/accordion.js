import React, { useState } from 'react';

import '../../css/ec/accordion.css';

function Accordion({ items }) {

    const [index, setIndex] = useState(0);
    const [collapse, setCollapase] = useState(false);

    return (
        <div className="accordion">
            {items.map(({ title, content, id}, arrayIndex) => {
                const activeCls = !collapse  && index === arrayIndex ? 'show' : '';

                return (
                    <div key={id} className="card">
                        <div
                            className="card-header"
                            onClick={() => {
                                setIndex(arrayIndex);
                                setCollapase(index === arrayIndex && !collapse);
                            }}
                        >
                            <h2 className="mb-0">
                                <button className="btn btn-link" type="button">
                                    {title}
                                </button>
                            </h2>
                        </div>

                        <div className={`collapse ${activeCls}`}>
                            <div className="card-body">{content}</div>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}

export default Accordion;
