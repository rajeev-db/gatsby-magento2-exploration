import { _inArray } from "../../helper";

const ProductSwatchModel = {
    getSwatchAttributeIds: function (swatchesSelected) {
        return (swatchesSelected || []).map(s => s.split('_')[0])
    },

    prepareSelectedSwatches: function (swatchesSelected, newSwatchToAdd) {
        // if the swatch already present, then remove it from the list
        if (_inArray(swatchesSelected, newSwatchToAdd)) {
            return swatchesSelected.filter(s => s !== newSwatchToAdd);
        }

        let newSwatches = [...swatchesSelected, newSwatchToAdd];
        const swatchAttributeIds = ProductSwatchModel.getSwatchAttributeIds(swatchesSelected);
        const newSwatchAttributeId = newSwatchToAdd.split('_')[0];

        // if a swatch option is already present against the swatch attribute, then remove that
        // and add new swatch entry
        if (_inArray(swatchAttributeIds, newSwatchAttributeId)) {
            newSwatches = [];

            swatchesSelected.forEach(s => {
                if (s.split('_')[0] !== newSwatchAttributeId) {
                    newSwatches.push(s);
                }
            });

            newSwatches.push(newSwatchToAdd);
        }

        return newSwatches;
    }
};

export default ProductSwatchModel;
