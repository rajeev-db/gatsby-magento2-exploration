import { _inArray } from "../helper";
import ProductSwatchModel from "./product/swatch";

const CONFIGURABLE_PRODUCT_TYPE = 'ConfigurableProduct';

const PdpModel = {
    validateMsg: '',

    validate: function({ swatchesSelected, swatchAttributes, qtySelected}) {
        PdpModel.validateMsg = '';

        if (!qtySelected) {
            PdpModel.validateMsg = 'Please specify the quantity';
            return false;
        }

        const swatchSelectedAttibutes = ProductSwatchModel.getSwatchAttributeIds(swatchesSelected);

        if (!swatchSelectedAttibutes || swatchAttributes.length !== swatchSelectedAttibutes.length) {
            PdpModel.validateMsg = 'Please fill the configurable options';
            return false;
        }

        swatchAttributes.forEach(swatchAttributeId => {
            if (!_inArray(swatchSelectedAttibutes, swatchAttributeId)) {
                PdpModel.validateMsg = 'Please fill the configurable options';
                return false;
            }
        });

        return true;
    },

    collectSelectedChildSku: function (product, swatchesSelected = []) {
        let sku = '';
        let indexCounter = 0;
        let swatchAttrbutes = {};
        const selectedSwatchAttrIds = [];
        const selectedSwatchOptions = [];
        const {
            __typename,
            variants: productVariants = [],
            configurable_options: productConfigOptions = []
        } = product;

        if (__typename !== CONFIGURABLE_PRODUCT_TYPE || !swatchesSelected.length) {
            return '';
        }



        // update array with attribute ids from selected swatches
        (swatchesSelected || []).forEach(swatch => {
            const { 0: attrId, 1: optionId } = swatch.split('_');
            selectedSwatchAttrIds.push(attrId);
            selectedSwatchOptions.push(optionId);
        });

        productConfigOptions.forEach(prodConfigOpt => {
            const { attribute_id: attrId, attribute_code: attrCode } = prodConfigOpt;

            if (_inArray(selectedSwatchAttrIds, attrId)) {
                swatchAttrbutes[attrCode] = {
                    id: attrId,
                    code: attrCode,
                    value: selectedSwatchOptions[indexCounter],
                };

                indexCounter += 1;
            }
        });

        // collect value index from product variation information
        productVariants.forEach(prodVariant => {
            let isVariantValid = true;
            const { product: { sku: variantSku }, attributes = [] } = prodVariant;

            attributes.forEach(attr => {
                if (!swatchAttrbutes[attr.code] ||
                    Number(swatchAttrbutes[attr.code]['value']) !== Number(attr.value_index)
                ) {
                    isVariantValid = false;
                }
            });

            if (isVariantValid) {
                sku = variantSku;
            }
         });

        return sku;
    },
};

export default PdpModel;
