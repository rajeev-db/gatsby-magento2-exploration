import React, { useState, useEffect } from "react";
import { connect } from 'react-redux';

import Layout from "./common/layout"
import Header from './common/header';
import Content from './common/content';
import ProductList from './product/list';

import { fetchCategoryProducts } from "../../store/actions";

function PLP({ productList }) {
    return (
        <Layout>
            <Header />
            <Content>
                <ProductList page={{}} items={productList} />
            </Content>
        </Layout>
    );
}

function mapStateToProps({ category: { categoryId } }) {
    return {
        categoryId,
    };
}

const mapDispatchToProps = {
    fetchProducts: fetchCategoryProducts
};

export default connect(mapStateToProps, mapDispatchToProps)(PLP);
