import React from 'react';

import { Layout, Header, Content, GlobalMessage } from './common';
import ProductMain from './product/main';

function PDP() {
    return (
        <>
            <Layout>
                <Header />
                <Content>
                    <GlobalMessage />
                    <ProductMain />
                </Content>
            </Layout>
        </>
    );
}

export default PDP;
