import React from 'react';
import { connect } from 'react-redux';

import MinicartItem from './minicart/item';

import { updateHeaderMinicartStatus, fetchCartDetails } from '../../../store/actions';

class MiniCart extends React.Component {
    constructor(props) {
        super(props);

        this.closeHandler = this.closeHandler.bind(this);
    }

    componentDidMount() {
        const { cartId, fetchCartInfo } = this.props;

        if (cartId) {
            fetchCartInfo();
        }
    }

    closeHandler(event) {
        event.preventDefault();

        const { updateMinicartStatus } = this.props;

        updateMinicartStatus(false);
    }

    render() {
        const {
            props: { cartId, status, cartItems, cartSubtotal, currency },
            closeHandler
        } = this;

        if (!status) {
            return <></>;
        }

        const cartContent = cartId &&
        (
            <>
                <div className="shp__cart__wrap">
                    {cartItems.map(
                        cartItem => <MinicartItem itemId={cartItem.id} key={cartItem.id} />
                    )}
                </div>
                <ul className="shoping__total">
                    <li className="subtotal">Subtotal:</li>
                    <li className="total__price">{`${currency}${cartSubtotal}`}</li>
                </ul>
                <ul className="shopping__btn">
                    <li><a href="cart.html">View Cart</a></li>
                    <li className="shp__checkout"><a href="checkout.html">Checkout</a></li>
                </ul>
            </>
        ) ||
        (
            <div className="shp__cart__wrap">There is no item in the cart.</div>
        );

        return (
            <div className="shopping__cart shopping__cart__on">
                <div className="shopping__cart__inner">
                    <div className="offsetmenu__close__btn">
                        <a href="close" onClick={closeHandler}><i className="zmdi zmdi-close"></i></a>
                    </div>
                    {cartContent}
                </div>
            </div>
        );
    }
}
function mapStateToProps(state) {
    const {
        header: { minicartStatus },
        cart: {
            cartId,
            info: {
                items: cartItems = { cartItems: [] },
                prices: cartPrices = {}
            },
        },
        store: { currencySymbol },
    } = state;

    const { subtotal_including_tax: subTotalInfo = {} } = cartPrices;

    return {
        status: minicartStatus,
        cartItems: cartItems,
        cartSubtotal: subTotalInfo.value || 0,
        currency: currencySymbol || subTotalInfo.currency || '',
        cartId,
    };
}

const mapDispatchToProps = {
    updateMinicartStatus: updateHeaderMinicartStatus,
    fetchCartInfo: fetchCartDetails,
};

export default connect(mapStateToProps, mapDispatchToProps)(MiniCart);
