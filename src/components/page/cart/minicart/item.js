import React from 'react';
import { connect } from 'react-redux';

function MinicartItem({ item, currencySymbol }) {
    const {
        product,
        prices: { price: priceInfo },
        product: { small_image: imageInfo },
    } = item;
    const currency = currencySymbol || priceInfo.currency;

    return (
        <div className="shp__single__product">
            <div className="shp__pro__thumb">
                <a href="#">
                    <img src={imageInfo.url} alt={imageInfo.label} />
                </a>
            </div>
            <div className="shp__pro__details">
                <h2><a href="#">{product.name}</a></h2>
                <span className="quantity">QTY: {item.quantity}</span>
                <span className="shp__price">{`${currency}${priceInfo.value}`}</span>
            </div>
            <div className="remove__btn">
                <a href="#" title="Remove this item"><i className="zmdi zmdi-close"></i></a>
            </div>
        </div>
    );
}

function mapStateToProps(state, { itemId }) {
    const {
        cart: {
            info: {
                items: cartItems = { cartItems: [] },
            },
        },
        store: {
            currencySymbol,
        },
    } = state;
    const cartItem = cartItems.find(item => item.id === itemId);

    return {
        item: cartItem,
        currencySymbol,
    };
}

export default connect(mapStateToProps)(MinicartItem);
