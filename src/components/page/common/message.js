import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import '../../../css/ec/message.css';
import { setGlobalMessage } from '../../../store/actions';

function GlobalMessage({ message : { type: messageType, message }, updateMessage }) {
    const [status, setStatus] = useState(false);
    useEffect(() => {
        setStatus(true);
        const timer = setTimeout(() => {
            setStatus(false);
            updateMessage('');
        }, 2000);

        return () => {
            clearTimeout(timer);
        };
    }, [message]);

    if (!status || !message) {
        return <></>;
    }

    return (
        <div className={`alert ${messageType}`}>
            <span className="closebtn" onClick={() => setStatus(false)}>&times;</span>
            {message}
        </div>
    );
}

const mapStateToProps = ({ message }) => ({ message });

const mapDispatchToProps = {
    updateMessage: setGlobalMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(GlobalMessage);
