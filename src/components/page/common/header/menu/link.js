import React from 'react';
import { Link } from '@reach/router';

function MenuLink({
    subLinks,
    item : {
        name,
        url_key: url = { url: 'ecommerce'},
    },
}) {
    return (
        <li className="drop">
            <Link to={`/${url}`}>{name}</Link>
            {subLinks}
        </li>
    );
}

export default MenuLink;
