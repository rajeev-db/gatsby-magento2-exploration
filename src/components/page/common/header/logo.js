import React from 'react';
import { Link } from '@reach/router';

import logo from '../../../../images/logo/logo.png';

function Logo() {
    return (
        <div className="col-md-2 col-lg-2 col-sm-3 col-xs-3">
            <div className="logo">
                <Link to="/ecommerce">
                    <img src={logo} alt="logo"/>
                </Link>
            </div>
        </div>
    );
}

export default Logo;
