import React from 'react';
import { connect } from 'react-redux';
import { updateHeaderMinicartStatus } from '../../../../store/actions';

class HeaderCart extends React.Component {
    constructor(props) {
        super(props);

        this.clickHandler = this.clickHandler.bind(this);
    }

    clickHandler() {
        const { updateMinicart, status } = this.props;

        updateMinicart(!status);
    }

    render() {
        return (
            <li className="cart__menu" onClick={this.clickHandler}>
                <span className="ti-shopping-cart"></span>
            </li>
        );
    }
}

function mapStateToProps({ header: { minicartStatus } }) {
    return {
        status: minicartStatus,
    };
}

const mapDispatchToProps = {
    updateMinicart: updateHeaderMinicartStatus,
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderCart);
