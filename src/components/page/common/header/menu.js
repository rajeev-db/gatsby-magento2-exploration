import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import MenuLink from './menu/link';

import { fetchCategoriesLevel3 } from '../../../../store/actions';
import { awaitFetch } from '../../../helper';

function Menu({ items, limit, fetchMenuCategories }) {
    useEffect(() => {
        awaitFetch(fetchMenuCategories());
    }, []);

    const slicedItems = items.slice(0, limit);

    const categoryMenu = slicedItems.map(item => {
        const subLinks = (
            <ul className="dropdown">
                {(item.children || []).map(subItem =>
                    subItem.include_in_menu &&
                    (<MenuLink
                        item={subItem}
                        key={`${item.id}_${subItem.id}`}
                        subLinks={<></>}
                    />) ||
                    (<></>)
                )}
            </ul>
        );
        return (
            <MenuLink item={item} key={item.id} subLinks={subLinks} />
        );
    })

    return (
        <div className="col-md-8 col-lg-8 col-sm-6 col-xs-6">
            <nav className="mainmenu__nav hidden-xs hidden-sm">
                <ul className="main__menu">
                    {categoryMenu}
                </ul>
            </nav>
            <div className="mobile-menu clearfix visible-xs visible-sm">
                <nav id="mobile_dropdown">
                    <ul>
                        {categoryMenu}
                    </ul>
                </nav>
            </div>
        </div>
    );
}

function mapStateToProps({ menu: { items } }) {
    return {
        items,
    };
}

const mapDispatchToProps = {
    fetchMenuCategories: fetchCategoriesLevel3,
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
