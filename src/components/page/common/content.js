import React from 'react';

function content({ children }) {
    return <div>{children}</div>;
}

export default content;
