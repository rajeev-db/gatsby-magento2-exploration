import React from 'react';

import '../../../css/bootstrap.min.css';
import '../../../css/core.css';
import '../../../css/shortcode/shortcodes.css';
import '../../../css/style.css';

function Layout({ children }) {
    return <div className="wrapper fixed__footer">{children}</div>;
}

export default Layout;
