import React from 'react';
import { connect } from 'react-redux';

import ProductListImage from './list/image';

function ProductList({ items, page, currencySymbol }) {
    return (
        <section className="htc__product__area shop__page ptb--130 bg__white">
            <div className="container">
                <div className="htc__product__container">
                    <div className="row">
                        <div className="product__list another-product-style">
                        {items.map(product => {
                            const minPriceinfo = product.price_range.minimum_price;
                            const regularPrice = minPriceinfo.regular_price.value;
                            const finalPrice = minPriceinfo.final_price.value;
                            return (
                                <div key={product.sku} className="col-md-3 single__pro col-lg-3 col-sm-4 col-xs-12 cat--4">
                                    <div className="product foo" data-sr-id="11">
                                        <div className="product__inner">
                                            <ProductListImage product={product} />
                                            <div className="product__hover__info">
                                                <ul className="product__action">
                                                    <li><a data-toggle="modal" data-target="#productModal" title="Quick View" className="quick-view modal-view detail-link" href="quick.html"><span className="ti-plus"></span></a></li>
                                                    <li><a title="Add TO Cart" href="cart.html"><span className="ti-shopping-cart"></span></a></li>
                                                    <li><a title="Wishlist" href="wishlist.html"><span className="ti-heart"></span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="product__details">
                                            <h2><a href={`${page.baseUrl}${product.canonical_url}`}>
                                                {product.name}
                                            </a></h2>
                                            <ul className="product__price">
                                                {regularPrice === finalPrice &&
                                                    <></> ||
                                                    (
                                                        <li className="old__price">
                                                            {`${currencySymbol}${regularPrice}`}
                                                        </li>
                                                    )
                                                }

                                                <li className="new__price">
                                                    {`${currencySymbol}${finalPrice}`}
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            );
                        })}
                        </div>
                    </div>


                    <div className="row mt--60">
                        <div className="col-md-12">
                            <div className="htc__loadmore__btn">
                                <a href="load.html">load more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

function mapStateToProps({ store: { currencySymbol }}) {
    return {
        currencySymbol,
    };
}

export default connect(mapStateToProps)(ProductList);
