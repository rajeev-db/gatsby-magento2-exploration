import React from 'react';
import { connect } from 'react-redux';
import { Link } from "@reach/router";

import { updatePDPCurrentProduct } from '../../../../store/actions';


function ProductListImage({
    product: {
        image,
        url_key: url,
    },
}) {
    return (
        <div className="pro__thumb">
            <Link to={`/${url}`}>
                <img src={image.url} alt={image.label} />
            </Link>
        </div>
    );
}


// class ProductListImage extends React.Component {
//     constructor(props) {
//         super(props);

//         this.imgClickHandler = this.imgClickHandler.bind(this);
//     }

//     imgClickHandler(event) {
//         event.preventDefault();

//         const { product: { sku, canonical_url: url }, updatePDPSku } = this.props;
//         updatePDPSku(sku);
//         navigate(`ecommerce/product`);
//     }

//     render() {
//         const {
//             props: {
//                 product: {
//                     image,
//                     canonical_url: url,
//                 },
//             },
//             imgClickHandler,
//         } = this;

//         return (
//             <div className="pro__thumb">
//                 <a href={`product/${url}`} onClick={imgClickHandler}>
//                     <img src={image.url} alt={image.label} />
//                 </a>
//             </div>
//         );
//     }
// }

const mapDispatchToProps = {
    updatePDPSku: updatePDPCurrentProduct,
};

export default connect(null, mapDispatchToProps)(ProductListImage);
