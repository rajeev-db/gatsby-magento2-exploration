import React from 'react';
import { connect } from 'react-redux';

import ProductGallery from './details/gallery';
import ProductRating from './details/rating';
import ProductSwatches from './details/swatches';
import ProductQtyBox from './details/qtybox';
import BuyNow from './details/buynow';
import CustomOptions from './details/options';

function ProductMain({ product, currencySymbol }) {
    if (!product.sku) {
        return <></>;
    }

    const priceInfo = product.price_range.minimum_price;
    const regularPrice = priceInfo.regular_price.value;
    const finalPrice = priceInfo.final_price.value

    return (
        <div className="wrapper fixed__footer">
            <section className="htc__product__details pt--20 pb--100 bg__white">
                <div className="container">
                    <div className="row">
                        <ProductGallery />
                        <div className="col-md-6 col-lg-6 col-sm-12 col-xs-12 smt-30 xmt-30">
                            <div className="htc__product__details__inner">
                                <div className="pro__detl__title">
                                    <h2>{product.name}</h2>
                                </div>
                                <ProductRating />
                                <ul className="pro__dtl__prize">
                                    {regularPrice === finalPrice && <></> || (
                                        <li className="old__prize">
                                            {`${currencySymbol}${regularPrice}`}
                                        </li>
                                    )}
                                    <li>{`${currencySymbol}${finalPrice}`}</li>
                                </ul>

                                <ProductSwatches attributes={product.configurable_options} />
                                <ProductQtyBox />
                                <CustomOptions />
                                <BuyNow />
                                <div className="pro__social__share">
                                    <h2>Share :</h2>
                                    <ul className="pro__soaial__link">
                                        <li><a href="#"><i className="zmdi zmdi-twitter"></i></a></li>
                                        <li><a href="#"><i className="zmdi zmdi-instagram"></i></a></li>
                                        <li><a href="#"><i className="zmdi zmdi-facebook"></i></a></li>
                                        <li><a href="#"><i className="zmdi zmdi-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>

        <section className="htc__product__details__tab bg__white pb--120">
            <div className="container">
                <div className="row">
                    <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <ul className="product__deatils__tab mb--60" role="tablist">
                            <li role="presentation" className="active">
                                <a href="#description" role="tab" data-toggle="tab">Description</a>
                            </li>
                            <li role="presentation">
                                <a href="#sheet" role="tab" data-toggle="tab">Data sheet</a>
                            </li>
                            <li role="presentation">
                                <a href="#reviews" role="tab" data-toggle="tab">Reviews</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="product__details__tab__content">
                            <div role="tabpanel" id="description" className="product__tab__content fade in active">
                                <div className="product__description__wrap">
                                    <div className="product__desc">
                                        <h2 className="title__6">Details</h2>
                                        <div className="pro__details" dangerouslySetInnerHTML={{ __html: product.description.html}} />
                                    </div>
                                    <div className="pro__feature">
                                        <h2 className="title__6">Features</h2>
                                        <ul className="feature__list">
                                            <li><a href="#"><i className="zmdi zmdi-play-circle"></i>Duis aute irure dolor in reprehenderit in voluptate velit esse</a></li>
                                            <li><a href="#"><i className="zmdi zmdi-play-circle"></i>Irure dolor in reprehenderit in voluptate velit esse</a></li>
                                            <li><a href="#"><i className="zmdi zmdi-play-circle"></i>Sed do eiusmod tempor incididunt ut labore et </a></li>
                                            <li><a href="#"><i className="zmdi zmdi-play-circle"></i>Nisi ut aliquip ex ea commodo consequat.</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" id="sheet" className="product__tab__content fade">
                                <div className="pro__feature">
                                        <h2 className="title__6">Data sheet</h2>
                                        <ul className="feature__list">
                                            <li><a href="#"><i className="zmdi zmdi-play-circle"></i>Duis aute irure dolor in reprehenderit in voluptate velit esse</a></li>
                                            <li><a href="#"><i className="zmdi zmdi-play-circle"></i>Irure dolor in reprehenderit in voluptate velit esse</a></li>
                                            <li><a href="#"><i className="zmdi zmdi-play-circle"></i>Irure dolor in reprehenderit in voluptate velit esse</a></li>
                                            <li><a href="#"><i className="zmdi zmdi-play-circle"></i>Sed do eiusmod tempor incididunt ut labore et </a></li>
                                            <li><a href="#"><i className="zmdi zmdi-play-circle"></i>Sed do eiusmod tempor incididunt ut labore et </a></li>
                                            <li><a href="#"><i className="zmdi zmdi-play-circle"></i>Nisi ut aliquip ex ea commodo consequat.</a></li>
                                            <li><a href="#"><i className="zmdi zmdi-play-circle"></i>Nisi ut aliquip ex ea commodo consequat.</a></li>
                                        </ul>
                                    </div>
                            </div>

                            <div role="tabpanel" id="reviews" className="product__tab__content fade">
                                <div className="review__address__inner">
                                    <div className="pro__review">
                                        <div className="review__thumb">
                                            <img src="images/review/1.jpg" alt="review images" />
                                        </div>
                                        <div className="review__details">
                                            <div className="review__info">
                                                <h4><a href="#">Gerald Barnes</a></h4>
                                                <ul className="rating">
                                                    <li><i className="zmdi zmdi-star"></i></li>
                                                    <li><i className="zmdi zmdi-star"></i></li>
                                                    <li><i className="zmdi zmdi-star"></i></li>
                                                    <li><i className="zmdi zmdi-star-half"></i></li>
                                                    <li><i className="zmdi zmdi-star-half"></i></li>
                                                </ul>
                                                <div className="rating__send">
                                                    <a href="#"><i className="zmdi zmdi-mail-reply"></i></a>
                                                    <a href="#"><i className="zmdi zmdi-close"></i></a>
                                                </div>
                                            </div>
                                            <div className="review__date">
                                                <span>27 Jun, 2016 at 2:30pm</span>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan egestas elese ifend. Phasellus a felis at estei to bibendum feugiat ut eget eni Praesent et messages in con sectetur posuere dolor non.</p>
                                        </div>
                                    </div>

                                    <div className="pro__review ans">
                                        <div className="review__thumb">
                                            <img src="images/review/2.jpg" alt="review images"/>
                                        </div>
                                        <div className="review__details">
                                            <div className="review__info">
                                                <h4><a href="#">Gerald Barnes</a></h4>
                                                <ul className="rating">
                                                    <li><i className="zmdi zmdi-star"></i></li>
                                                    <li><i className="zmdi zmdi-star"></i></li>
                                                    <li><i className="zmdi zmdi-star"></i></li>
                                                    <li><i className="zmdi zmdi-star-half"></i></li>
                                                    <li><i className="zmdi zmdi-star-half"></i></li>
                                                </ul>
                                                <div className="rating__send">
                                                    <a href="#"><i className="zmdi zmdi-mail-reply"></i></a>
                                                    <a href="#"><i className="zmdi zmdi-close"></i></a>
                                                </div>
                                            </div>
                                            <div className="review__date">
                                                <span>27 Jun, 2016 at 2:30pm</span>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan egestas elese ifend. Phasellus a felis at estei to bibendum feugiat ut eget eni Praesent et messages in con sectetur posuere dolor non.</p>
                                        </div>
                                    </div>

                                </div>

                                <div className="rating__wrap">
                                    <h2 className="rating-title">Write  A review</h2>
                                    <h4 className="rating-title-2">Your Rating</h4>
                                    <div className="rating__list">

                                        <ul className="rating">
                                            <li><i className="zmdi zmdi-star-half"></i></li>
                                        </ul>

                                        <ul className="rating">
                                            <li><i className="zmdi zmdi-star-half"></i></li>
                                            <li><i className="zmdi zmdi-star-half"></i></li>
                                        </ul>

                                        <ul className="rating">
                                            <li><i className="zmdi zmdi-star-half"></i></li>
                                            <li><i className="zmdi zmdi-star-half"></i></li>
                                            <li><i className="zmdi zmdi-star-half"></i></li>
                                        </ul>

                                        <ul className="rating">
                                            <li><i className="zmdi zmdi-star-half"></i></li>
                                            <li><i className="zmdi zmdi-star-half"></i></li>
                                            <li><i className="zmdi zmdi-star-half"></i></li>
                                            <li><i className="zmdi zmdi-star-half"></i></li>
                                        </ul>

                                        <ul className="rating">
                                            <li><i className="zmdi zmdi-star-half"></i></li>
                                            <li><i className="zmdi zmdi-star-half"></i></li>
                                            <li><i className="zmdi zmdi-star-half"></i></li>
                                            <li><i className="zmdi zmdi-star-half"></i></li>
                                            <li><i className="zmdi zmdi-star-half"></i></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="review__box">
                                    <form id="review-form">
                                        <div className="single-review-form">
                                            <div className="review-box name">
                                                <input type="text" placeholder="Type your name"/>
                                                <input type="email" placeholder="Type your email"/>
                                            </div>
                                        </div>
                                        <div className="single-review-form">
                                            <div className="review-box message">
                                                <textarea placeholder="Write your review"></textarea>
                                            </div>
                                        </div>
                                        <div className="review-btn">
                                            <a className="fv-btn" href="#">submit review</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        </div>
    );
}

function mapStateToProps({ pdp, store }) {
    return {
        currencySymbol: store.currencySymbol,
        product: pdp.productList[pdp.sku] || {},
    };
}

export default connect(mapStateToProps)(ProductMain);
