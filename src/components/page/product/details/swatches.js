import React from 'react';
import { connect } from 'react-redux';

import { addPDPSwatchSelected } from '../../../../store/actions';
import { _inArray } from '../../../helper';

const SWATCH_COLOR_ATTRIBUTE = 'color';
const highlighSwatchStyle = {
    color: 'red',
    fontWeight: 'bold',
};
const swatchColorBorderStyle = {
    borderBottom: '2px solid red',
    width: 15,
};

class ProductSwatches extends React.Component {
    swatchClickHandler(swatchId, event) {
        event.preventDefault();
        const { addToSwatchSelected } = this.props;

        addToSwatchSelected(swatchId);

    }

    render() {
        const {
            props: { attributes, swatchesSelected },
            swatchClickHandler
        } = this;

        return (attributes || []).map(attribute => {
            const { label: attrLabel, attribute_code: attrCode, attribute_id: attrId } = attribute;
            return (
                <div key={attrCode} className="pro__dtl__size">
                    <h2 className="title__5">{attrLabel}</h2>
                    <ul className={`pro__choose__${attribute.attribute_code.toLowerCase()}`}>
                        {(attribute.values || []).map(value => {
                            let swatchId;
                            let swatchStyle = {};
                            let colorSwatchBorder = {};
                            const { label: swatchLabel, value_index: swatchIndex } = value;
                            swatchId = `${attrId}_${swatchIndex}`;

                            if (_inArray(swatchesSelected, swatchId)) {
                                swatchStyle = highlighSwatchStyle;
                                if (attrCode === SWATCH_COLOR_ATTRIBUTE) {
                                    colorSwatchBorder = swatchColorBorderStyle;
                                }
                            }

                            return (
                                <li
                                    key={swatchId}
                                    className={swatchLabel.toLowerCase()}
                                    style={colorSwatchBorder}
                                >
                                    <a
                                        href={swatchId}
                                        onClick={swatchClickHandler.bind(this, swatchId)}
                                        style={swatchStyle}

                                    >
                                        {attrCode !== SWATCH_COLOR_ATTRIBUTE && swatchLabel || (
                                            <i className="zmdi zmdi-circle"></i>
                                        )}
                                    </a>
                                </li>
                            );
                        })}
                    </ul>
                </div>
            );
        });
    }
}

function mapStateToProps({ pdp }) {
    return {
        swatchesSelected: pdp.swatchesSelected,
    }
}

const mapDispatchToProps = {
    addToSwatchSelected: addPDPSwatchSelected,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductSwatches);
