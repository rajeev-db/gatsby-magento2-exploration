import React, { useState } from 'react';
import { connect } from 'react-redux';

import styles from '../../../../css/ec/product/gallery.module.css';

function ProductGallery({ product }) {
    const { mainImageUrl, mediaGallery } = product;
    const defaultmainImgIndex = mediaGallery.findIndex(mg => mg.url === mainImageUrl);
    const [mainImgIndex, setMainImgIndex] = useState(defaultmainImgIndex);

    return (
        <div className="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div className="product__details__container">
                <ul className="product__small__images" role="tablist">
                    {mediaGallery.map(({ url: mediaImgUrl }, index) => {
                        const imageIndex = `#img-tab-${index}`;
                        const activeCls = mainImgIndex ? 'active': '';

                        return (
                            <li key={imageIndex} className={`pot-small-img ${activeCls}`}>
                                <a
                                    href={imageIndex}
                                    className={styles.previewSmallImg}
                                    onClick={(event) => {
                                        event.preventDefault();
                                        setMainImgIndex(index);
                                    }}
                                >
                                    <img src={mediaImgUrl} alt="small-image"/>
                                </a>
                            </li>
                        );
                    })}
                </ul>
                <div className={`product__big__images ${styles.previewMainImg}`}>
                    <div className="portfolio-full-image tab-content">
                        <div className="tab-pane fade in active product-video-position">
                            <img
                                src={mediaGallery[mainImgIndex] && mediaGallery[mainImgIndex].url || mainImageUrl}
                                alt="main-image"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

function mapStateToProps({
    pdp: {
        sku,
        productList,
    }
}) {
    const {
        media_gallery: mediaGallery = [],
        image: {
            url: mainImageUrl = '',
        }
    } = productList[sku] || {};

    return {
        product: {
            sku,
            mediaGallery,
            mainImageUrl,
        }
    };
}

export default connect(mapStateToProps)(ProductGallery);
