import React from 'react';
import { connect } from 'react-redux';

import { updatePDPQtySelected } from '../../../../store/actions';

class ProductQtyBox extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            qty: 1,
        };

        this.changeQtyHandler = this.changeQtyHandler.bind(this);
        this.incrementHandler = this.incrementHandler.bind(this);
        this.decrementHandler = this.decrementHandler.bind(this);
        this.updateQtyState= this.updateQtyState.bind(this);
    }
    componentDidMount() {
        const {
            state: { qty },
            props: { updateQtySelected },
        } = this;

        updateQtySelected(qty);

    }

    updateQtyState(qty) {
        const { updateQtySelected } = this.props;

        this.setState({ qty })
        updateQtySelected(qty);
    }

    changeQtyHandler(event) {
        this.updateQtyState(event.target.value || 1);
    }

    incrementHandler() {
        const {
            state: { qty },
            updateQtyState,
         } = this;

         updateQtyState(qty + 1);
    }

    decrementHandler() {
        const {
            state: { qty },
            updateQtyState,
         } = this;

         updateQtyState(qty - 1 || 1);
    }

    render() {

        const {
            state: { qty },
            changeQtyHandler,
            incrementHandler,
            decrementHandler,
        } = this;

        return (
            <div className="product-action-wrap">
                <div className="prodict-statas"><span>Quantity :</span></div>
                <div className="product-quantity">
                    <form id='myform' method='POST' action='#'>
                        <div className="product-quantity">
                            <div className="cart-plus-minus">
                                <input
                                    className="cart-plus-minus-box"
                                    type="text"
                                    name="qtybutton"
                                    value={qty}
                                    onChange={changeQtyHandler}
                                />
                                <div className="dec qtybutton" onClick={decrementHandler}>-</div>
                                <div className="inc qtybutton" onClick={incrementHandler}>+</div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = {
    updateQtySelected: updatePDPQtySelected,
}

export default connect(null, mapDispatchToProps)(ProductQtyBox);
