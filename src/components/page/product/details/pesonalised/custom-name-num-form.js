import React, { useState } from 'react';
import { connect } from 'react-redux';

import styles from '../../../../../css/ec/product/custom-options.module.css';
import { _removeNums } from '../../../../helper/string';

const BUTTON_CLS = `btn ${styles.button}`;
const SKU_CUSTOM_NUMBER = 'CNUMBER';
const SKU_CUSTOM_NAME = 'CNAME';
const PLACE_HOLDERS = {
    [SKU_CUSTOM_NAME]: 'Allow only latin characters',
    [SKU_CUSTOM_NUMBER]: '',
};

function CustomNameNumberForm({ productOptions }) {
    const inputElems = [];
    const optionSkus = [];
    let badgeElem = <></>;

    const [inputs, setInputs] = useState({});

    productOptions.forEach(({
        option_id: optionId,
        title: optionTitle,
        value: optionValue = {},
        radioOptions = [],
    }) => {
        const {
            max_characters: maxCharacters = '',
            sku: optionSku = '',
        } = optionValue;

        if (optionSku) {
            optionSkus.push(optionSku);
            inputElems.push({
                optionId,
                optionTitle,
                maxCharacters,
                optionSku,
            });
        }

        const {
            0: yesOption = { yesOption: {} },
        } = radioOptions;

        if (yesOption.option_type_id) {
            badgeElem = (
                <div key={optionId} className="form-group">
                    <label>Badge</label>
                    <div>
                        <button type="button" className={`${BUTTON_CLS} btn-success`}>
                            {yesOption.title}
                        </button>
                        <button type="button" className={`${BUTTON_CLS} btn-danger`}>NO</button>
                    </div>
                </div>
            );
        }
    });

    const updateInputValue = event => {
        const {
            target: {
                value,
                name
            }
        } = event;
        let inpValue = value;

        if (name === SKU_CUSTOM_NUMBER) {
            // removes letters other than numbers from input
            // @todo cannot make this function as a helper now. not sure why. some wierd error comming
            inpValue = Array.from(value, s => !!Number(s) ? s: '').join('');
        }

        setInputs({
            ...inputs,
            [name]: inpValue
        });
    };

    return (
        <form className={styles.form}>
            {inputElems.map(({
                optionId,
                optionTitle,
                maxCharacters,
                optionSku,
            }) => {
                const inputMsg = `Maximum allowed letters: ${maxCharacters}`;

                return (
                    <div key={optionId} className="form-group">
                        <label htmlFor={optionSku}>{optionTitle}</label>
                        <input
                            type="text"
                            className="form-control form-control-lg"
                            id={optionSku}
                            placeholder={PLACE_HOLDERS[optionSku] || ''}
                            maxLength={maxCharacters}
                            name={optionSku}
                            onChange={updateInputValue}
                            value={inputs[optionSku] || ''}
                        />
                        <small id={optionId} className="form-text text-muted">{inputMsg}</small>
                    </div>
                );
            })}

            {badgeElem}
        </form>
    );
}

function mapStateToProps({
    pdp: {
        sku,
        productList,
    }
}) {

    return {
        productOptions: productList[sku] && productList[sku].options || [],
    }
}

export default connect(mapStateToProps)(CustomNameNumberForm);
