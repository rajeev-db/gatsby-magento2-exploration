import React from 'react';
import { connect } from 'react-redux';

import PdpModel from '../../../model/pdp';
import { WARNING_MESSAGE, SUCCESS_MESSAGE, ERROR_MESSAGE } from '../../../model/message';

import {
    setGlobalMessage,
    updateHeaderMinicartStatus,
    createEmptyCart,
    addProductoCart,
} from '../../../../store/actions';

class BuyNow extends React.Component {
    constructor(props) {
        super(props);

        this.buyNowHandler =  this.buyNowHandler.bind(this);
        this.addToCart =  this.addToCart.bind(this);
    }

    buyNowHandler(event) {
        event.preventDefault();

        const { props, props: { updateMessage }, addToCart } = this;

        if (PdpModel.validate(props)) {
            addToCart();
        } else {
            updateMessage(PdpModel.validateMsg, WARNING_MESSAGE);
        }
    }

    addToCart() {
        const {
            cartId,
            qtySelected,
            childSku,
            productSku,
            createNewCart,
            addProductToTheCart,
            updateMinicartStatus,
            updateMessage,
        } = this.props;
        const cartDetails = {
            cartId,
            parentSku: productSku,
            qty: qtySelected,
            sku: childSku,
        }

        const successCallback = () => {
            updateMinicartStatus(true);
            updateMessage('Product added to the cart successfully', SUCCESS_MESSAGE);
        }
        const errorCallback = error => {
            updateMessage(
                'We could not add product to the cart, pleas try again.',
                ERROR_MESSAGE
            );
        }


        if (cartId) {
            addProductToTheCart(cartDetails).then(successCallback).catch(errorCallback);
        } else {
            createNewCart(false).then(newCartId => {
                addProductToTheCart({ ...cartDetails, cartId: newCartId })
                .then(successCallback)
                .catch(errorCallback);
            });
        }
    }

    render() {
        const { buyNowHandler } = this;

        return (
            <ul className="pro__dtl__btn">
                <li className="buy__now__btn">
                    <a href="buynow" onClick={buyNowHandler}>buy now</a>
                </li>
                <li><a href="ti-heart.html"><span className="ti-heart"></span></a></li>
                <li><a href="ti-email.html"><span className="ti-email"></span></a></li>
            </ul>
        );
    }
}

function mapStateToProps({ pdp, cart }) {
    const { cartId } = cart;
    const {
        swatchAttributeIds: swatchAttributes,
        swatchesSelected,
        qtySelected,
        productList,
        sku
    } = pdp;

    return {
        swatchAttributes,
        swatchesSelected,
        qtySelected,
        productSku: sku,
        cartId,
        childSku: PdpModel.collectSelectedChildSku(productList[sku], swatchesSelected),
    };
}

const mapDispatchToProps = {
    createNewCart: createEmptyCart,
    addProductToTheCart: addProductoCart,
    updateMinicartStatus: updateHeaderMinicartStatus,
    updateMessage: setGlobalMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(BuyNow);
