import React from 'react';
import { connect } from 'react-redux';

import { Accordion } from '../../../common';
import { CustomNameNumberForm, ShopByPlayerList } from './pesonalised';

const personaliseCustomOptionAccordionItems = [
    {
        id: 'personalise_shirt',
        title: 'Personalise Your Shirt',
        content: <CustomNameNumberForm />,
    },
    {
        id: 'shop_by_players',
        title: 'Shop By Player',
        content: <ShopByPlayerList />,
    }
];

function CustomOptions({ isPersonalised }) {
    if (isPersonalised) {
        return <Accordion items={personaliseCustomOptionAccordionItems} />
    }

    /**
     * @todo here comes the logic to show normal custom options; should implement later
     */
    return <></>;
};

function mapStateToProps({
    pdp: {
        sku,
        productList,
    }
}) {
    return {
        isPersonalised: productList[sku] && productList[sku].is_personalised
    };
}

export default connect(mapStateToProps)(CustomOptions);
