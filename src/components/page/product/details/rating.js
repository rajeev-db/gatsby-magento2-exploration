import React from 'react';

function ProductRating() {
    return (
        <div className="pro__dtl__rating">
            <ul className="pro__rating">
                <li><span className="ti-star"></span></li>
                <li><span className="ti-star"></span></li>
                <li><span className="ti-star"></span></li>
                <li><span className="ti-star"></span></li>
                <li><span className="ti-star"></span></li>
            </ul>
            <span className="rat__qun">(Based on 0 Ratings)</span>
        </div>
    );
}

export default ProductRating;
