const { gql } = require('apollo-boost');

module.exports.COLLECT_GENERAL = gql`
  {
    currency {
      default_display_currency_symbol
    }
    categoryList(filters: {ids: {eq: "2"}}) {
      id
      children {
        name
        id
        include_in_menu
        level
        url_key
        children {
          name
          id
          include_in_menu
          level
          url_key
        }
      }
    }
  }
`;

module.exports.COLLECT_CATEGORY_PRODUCTS =  gql`
query getCategoryProducts($ids: [String!]) {
  categoryList(filters: { ids: { in: $ids } }) {
    id
    url_key
    products(pageSize: 16, sort: { name: ASC }) {
      items {
            id
            sku
            __typename
            name
            url_key
          description {
                html
            }
            image {
                label
                url
            }
            price_range {
                minimum_price {
                    final_price {
                        currency
                        value
                    }
                    regular_price {
                        currency
                        value
                    }
                }
            }

        }
    }
  }
}
`;

module.exports.COLLECT_PRODUCT_DETAILS =  gql`
query getProductDetailsBySku($skus: [String!]!, $page: Int!) {
    products(filter: {sku: {in: $skus } }, pageSize: $page) {
        items {
            id
            sku
            __typename
            name
            url_key
          description {
                html
            }
            image {
                label
                url
            }
            price_range {
                minimum_price {
                    final_price {
                        currency
                        value
                    }
                    regular_price {
                        currency
                        value
                    }
                }
            }
        }
    }
  }
`;
