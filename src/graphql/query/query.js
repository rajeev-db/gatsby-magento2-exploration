import { gql } from "apollo-boost";

export const QUERY_CATEGORY_PRODUCT_IDS = gql`
    {
        products(pageSize: 16, sort: {name: DESC}, filter: {category_id: {eq: "27"}}) {
            items {
                sku
                canonical_url
            }
        }
    }
`;

export const QUERY_GET_CATEGORY_PRODUCTS = gql`
    query getCategoryProducts($categoryId: String!){
      currency {
          default_display_currency_symbol
      }
      products(filter: {category_id: {eq: $categoryId } }, pageSize: 16, sort: { name: ASC }) {
          items {
              sku
              name
              url_key
              image {
                  label
                  url
              }
              price_range {
                  minimum_price {
                  final_price {
                      currency
                      value
                  }
                  regular_price {
                      currency
                      value
                  }
                  }
              }
          }
      }
  }
`;

export const QUERY_GET_PRODUCT_DETAILS_BY_SKU =  gql`
  query getProductDetailsBySku($productSku: String!) {
    currency {
        default_display_currency_symbol
    }
    products(filter: {sku: {eq: $productSku } }, pageSize: 1) {
        items {
            id
            sku
            __typename
            name
            url_key
            is_personalised
          description {
                html
            }
            media_gallery {
              label
              url
            }
            image {
                label
                url
            }
            price_range {
                minimum_price {
                    final_price {
                        currency
                        value
                    }
                    regular_price {
                        currency
                        value
                    }
                }
            }
            ... on ConfigurableProduct {
              configurable_options {
              attribute_id
              label
              attribute_code
              values {
                  value_index
                  label
              }
              product_id
              }
              variants {
                  product {
                      sku
                  }
                  attributes {
                      label
                      code
                      value_index
                  }
              }
          }

          ...on CustomizableProductInterface {
            options {
              option_id
              required
              sort_order
              title
             ...on CustomizableFieldOption {
              product_sku
              value {
                max_characters
                price
                price_type
                sku
              }
            }
              ...on CustomizableRadioOption {
                radioOptions: value {
                  option_type_id
                  price
                  price_type
                  sku
                  sort_order
                  title
                }
              }
            }
          }

        }
    }
  }
`;

export const QUERY_GET_CART_INFO = gql`
query getCart($cartId: String!) {
  cart(cart_id: $cartId) {
    id
    email
    items {
      id
      quantity
      product {
        id
        sku
        canonical_url
        name
        small_image {
          label
          url
        }
      }
      prices {
        price {
          currency
          value
        }
      }
      ... on ConfigurableCartItem {
        configurable_options {
          id
          option_label
          value_id
          value_label
        }
      }
    }
    prices {
      subtotal_including_tax {
        currency
        value
      }
    }
    total_quantity
  }
}
`;

export const CREATE_GUEST_EMPTY_CART = gql`
    mutation {
        createEmptyCart
    }
`;

export const CREATE_CUSTOMER_EMTPY_CART = gql`
    {
        customerCart {
            id
        }
    }
`;

export const ADD_CONFIG_PRODUCT_TO_CART = gql`
  mutation addConfigProductToCart(
    $cartId: String!,
    $parentSku: String!,
    $qty: Float!,
    $sku: String!
  ) {
      addConfigurableProductsToCart(
      input: {
          cart_id: $cartId
          cart_items: [
          {
              parent_sku: $parentSku
              data: {
              quantity: $qty
              sku: $sku
              }
          }
          ]
      }
      ) {
      cart {
            id
            email
            items {
            id
            quantity
            product {
                id
                sku
                canonical_url
                name
                small_image {
                label
                url
                }
            }
            prices {
                price {
                currency
                value
                }
            }
            ... on ConfigurableCartItem {
                configurable_options {
                id
                option_label
                value_id
                value_label
                }
            }
            }
            prices {
            subtotal_including_tax {
                currency
                value
            }
            }
            total_quantity
        }
      }
  }
`;

export const QUERY_LEVEL_3_CATEGORY_LIST = gql`
    {
    categoryList(filters: {ids: {eq: "2"}}) {
     id
      children {
        name
        id
        include_in_menu
        level
        url_key
        children {
          name
          id
          include_in_menu
          level
          url_key
        }
      }
    }
  }
`;

// export const query = graphql`
//   query ProductListQuery($productSkus: [String!]!) {
//     site {
//         siteMetadata {
//             siteURL
//         }
//     },
//     lfc {
//         products(filter: {sku: {in: $productSkus}}) {
//             items {
//                 sku
//                 name
//                 canonical_url
//                 image {
//                     label
//                     url
//                 }
//                 price_range {
//                     minimum_price {
//                     final_price {
//                         currency
//                         value
//                     }
//                     regular_price {
//                         currency
//                         value
//                     }
//                     }
//                 }
//             }
//         }
//     }
//   }
// `
