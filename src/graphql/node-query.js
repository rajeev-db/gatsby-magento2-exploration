const { gql } = require('apollo-boost');

const GET_ALL_CATEGORIES = gql`
{
    categoryList(filters: {ids: {eq: "2"}}) {
     id
      children {
        name
        id
        include_in_menu
        url_key
        children {
          name
          id
          include_in_menu
          level
          url_key
        }
      }
    }
  }
`;

const GET_ALL_PRODUCTS = gql`
{
  products (
    filter:{
      sku: {
        in: []
      }
    }
    pageSize: 10000
  ) {
    items {
      sku
      name
      url_key
    }
  }
}
`;

module.exports = {
    GET_ALL_CATEGORIES,
    GET_ALL_PRODUCTS,
};
