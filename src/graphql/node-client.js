const { HttpLink, InMemoryCache } = require("apollo-boost");
const { ApolloClient } = require('apollo-client');
// const { IntrospectionFragmentMatcher } = require('apollo-cache-inmemory');
const fetch = require('isomorphic-fetch');


// const introspectionQueryResultData = require('./fragmentTypes.json');

const uri = 'http://m24.test/graphql';

//const fragmentMatcher = new IntrospectionFragmentMatcher({ introspectionQueryResultData });
const link = new HttpLink({ uri, fetch });
const cache = new InMemoryCache();

const client = new ApolloClient({ link, cache });

module.exports = client;
