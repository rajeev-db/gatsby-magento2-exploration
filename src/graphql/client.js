import { HttpLink, InMemoryCache } from 'apollo-boost';
import { ApolloClient } from 'apollo-client';
import { IntrospectionFragmentMatcher } from 'apollo-cache-inmemory';
import fetch from 'isomorphic-fetch';

import introspectionQueryResultData from './fragmentTypes.json';


const uri = 'http://m24.test/graphql';

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData
});
const link = new HttpLink({ uri, fetch });
const cache = new InMemoryCache({ fragmentMatcher });

const client = new ApolloClient({ link, cache });

export default client;
