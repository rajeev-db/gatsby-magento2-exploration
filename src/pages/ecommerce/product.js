import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

import { PDP } from '../../components/page';

import {
    fetchProductDetails,
    updatePDPCurrentProduct
} from '../../store/actions';


function ProductPage({
    pdpSku,
    pageContext: { pageProductSku },
    fetchProduct,
    updatePDPSku,
}) {

    useEffect(() => {
        updatePDPSku(pageProductSku);
    }, [pageProductSku]);

    const [loading, setLoading] = useState(false);

    // performs only cDM & cWM; fetching product information here. needs to do this only once
    useEffect(() => {
        if (pdpSku) {
            setLoading(true);

            fetchProduct(pdpSku).then(() => {
                setLoading(false);
            });
        }
    }, [pdpSku]);

    if (loading) {
        return <>loading...</>;
    }

    return <PDP />;
}

function mapStateToProps({ pdp }) {
    const { sku } = pdp;

    return {
        pdpSku: sku,
    };
}

const mapDispatchToProps = {
    fetchProduct: fetchProductDetails,
    updatePDPSku: updatePDPCurrentProduct,
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductPage);
