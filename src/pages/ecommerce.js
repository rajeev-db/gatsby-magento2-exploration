import React, { useEffect, useState } from "react";
import { connect } from 'react-redux';

import { PLP } from '../components/page';

import { updateCategoryId, fetchCategoryProducts } from "../store/actions";

const DEFAULT_CATEGORY = 5;

function Ecommerce({
    categoryId,
    initProducts,
    pageContext : { pageCategoryId = DEFAULT_CATEGORY },
    updateCategory,
    fetchProducts,
}) {
    //cDM
    useEffect(() => {
        updateCategory(pageCategoryId);
    }, [pageCategoryId]);

    const [productList, setProductList] = useState(initProducts);
    useEffect(() => {
        fetchProducts().then(({ items }) => {
            if (items.length) {
                setProductList(items);
            }
        });
    }, [categoryId]);

    return <PLP productList={productList} />;
}

function mapStateToProps({ category: { categoryId, products } }) {
    return {
        categoryId,
        initProducts: products,
    };
}

const mapDispatchToProps = {
    updateCategory: updateCategoryId,
    fetchProducts: fetchCategoryProducts,
}

export default connect(mapStateToProps, mapDispatchToProps)(Ecommerce);
